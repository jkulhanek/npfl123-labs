Dotazovač na basketbalové zápasy
===

1.
* **S:** Jaká soutěž vás zajímá?
* **U:** Přebor B.
* **S:** Mužská nebo ženská soutěž?
* **U:** Mužská.
* **S:** Co chcete vědět o soutěži *muži - Přebor B 2019/2020*?
* **U:** Kdo je první v tabulce?
* **S:** Na prvním místě je tým *Slovan Bohnice A*.
* **U:** Změň sezónu na 2018.
* **S:** Co chcete vědět o soutěži *muži - Přebor B 2018/2019*?
* **U:** Kolikátý je tým Slovan Bohnice A?
* **S:** Tým *Slovan Bohnice A* není účastníkem soutěže *muži - Přebor B 2018/2019*.

2.
* **U:** Změň výchozí tým na VSK MFF UK A.
* **S:** Výchozí tým nastaven na *VSK MFF UK A*.
* **U:** Kdy je můj další zápas?
* **S:** Nejbližší utkání je *VSK MFF UK A* - *TJ ČSA* dne *02.03.2020* v *18:40*.
* **U:** Kde se hraje?
* **S:** Adresa utkání je *Bruslařská 10, Praha 10*.
* **U:** Chci se dozvědět něco více o týmu TJ ČSA.
* **S:** Co vás zajímá o týmu *TJ ČSA*?
* **U:** Kdo je nejlepší sřelec?
* **S:** V týmu *TJ ČSA* má nejvyšší počet bodů na zápas hráč *Karel Orel*, *21.7*.

3.
* **U:** Kolik má můj tým výher?
* **S:** Tým *VSK MFF UK A* má *10* výher.
* **U:** Který tým je na 8. místě?
* **S:** Na *8*. místě je tým *Sokol Kbely B*.
* **U:** Kolik má výher?
* **S:** Tým *Sokol Kbely B* má *8* výher.
* **U:** Kolik zápasů zbývá do konce?
* **S:** Do konce základní části soutěže *muži - Přebor B 2019/2020* zbývá *5* kol.
* **U:** Kolik týmů má méně než 3 výhry.
* **S:** Méně než *3* výhry má *1* tým. Je to tým *VŠTJ Ekonom Praha* s *2* výhrami.

4.
* **U:** Který hráč má nejlepší úspěšnost střelby trestných hodů?
* **S:** V týmu *VSK MFF UK A* má nejvyšší úspěšnost střelby trestných hodů hráč *Jan Trojan*, *83.3*%.
* **U:** Kdo dal nejvíc trojek?
* **S:** V týmu *VSK MFF UK A* má nejvyšší počet daných tříbodových pokusů hráč *Jan Trojan*, *25*.
* **U:** Kolik má *Jan Trojan* odehraných zápasů?
* **S:** Hráč *Jan Trojan* nastoupil ve *14* zápasech.
* **U:** Kdo je *Jan Trojan*?
* **S:** Hráč *Jan Trojan*, ročník narození *1996*, výška *201* cm.
* **U:** Kde bydlí *Jan Trojan*?
* **S:** Nerozumím.

5.
* **S:** Jaká soutěž vás zajímá?
* **U:** Extraliga mužů.
* **S:** Co chcete vědět o soutěži *Kooperativa NBL*?
* **U:** Jaké se dnes hrají zápasy?
* **S:** Dne *1.3.2020* se hrají *2* utkání. *BK Opava - USK Praha*, *Olomoucko - NH Ostrava*.
* **U:** Kolikátá je Ostrava?
* **S:** Tým *NH Ostrava* je na *10*. místě.
* **U:** Kolikátá je Ostrava?
* **S:** Tým *BK Opava* je na 2. místě.
* **U:** Kde se hraje utkání Opava proti USK?
* **S:** Adresa utkání *BK Opava - USK Praha, 1.3.2020* je *Pátkova 3, Opava*.
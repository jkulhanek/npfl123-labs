# Assignment 1
## Task 1
Domains: computer assistant, Solar System educational assistant.

## Task 2
For the first domain, I wrote the following example:
* A: Hello, how can I help you?
* U: I need to send a mail to a friend of mine.
* A: Ok, who do you want to sent it to?
* U: Oh, and I want edit the text of the message in Word.
* A: Ok, opening MS Word. Who do you want to send your email to?
* U: To Scarlett.
* A: Do you mean Scarlett Johansson?
* U: Yes.
* U: Ok, I have finished my message.
* A: What should be the subject of your email?
* U: Keep it blank.
* A: Do you want to attach something or sign your email?
* U: No, send it.
* A: Ok, sending.

- A: Hello, how can I help you?
- U: Can you list my repo directory?
- A: Sure, you have the following files: ... Do you want to show me also hidden files?
- U: No.
- A: Ok, what should I do next?
- U: Please, copy file A to directory B.
- A: Ok, what should I do next?
- U: Could you list directory B?
- A: Sure, you have the following files: ... Do you want to show me also hidden files?
- U: Thanks.
- A: Anything else?
- U: No.

- A: Hello, how can I help you?
- U: Do I have any unread emails?
- A: No new emails in your inbox.
- U: What about spam?
- A: There is one new email from abc@spamsender.com. Do you want me to open it?
- U: No, delete it please?
- A: Sure, anything else?
- U: No.


- A: Hello, how can I help you?
- U: I need to send new email?
- A: Sure, who do you want to send it to?
- U: Send the email to my boss with the message saying that I won't be able to go to work tomorrow.
- A: Sorry, what is the message?
- U: Hi Tom, unfortunatelly, I cannot go to work tomorrow. I am feeling very ill.
- A: What should be the subject of your email?
- U: Illness.
- A: Do you want to attach something or sign your email?
- U: Sign it with my signature and send it.
- A: Ok, sending.


- A: Hello, how can I help you?
- U: I want to erase all the files in the home directory.
- A: Do you really want to erase all files in the home directory?
- U: Yes.
- A: Sorry, I cannot finish the request.
- U: What is the problem?
- A: You have some protected files in your home directory. I couldn't remove files: ...
- U: Ok, remove everything else.
- A: Do you really want to erase the following files: ... from your home directory?
- U: Yes.
- A: Ok, erasing.
- A: Erased.

For the second domain, I wrote the following example:
- A: Hello, I know something about our solar system. What do you want to ask?
- U: I want to ask about our planet?
- A: Do you mean planet Earth?
- U: Yes.
- A: What is its size?
- A: In terms of the diameter it would be 6378 kilometers, however, planet Earth is not a perfect sphere, but more of a flatenned sphere.
- U: How many planets are smaller then Earth?
- A: It would be 3. Mars, Mercury and Venus.
- U: How far is Venus from us.
- A: The distance ranges between 38 million kilometers and 261 million kilometers depending on their relative orbital offset. Currently, it is 132 million kilometers far from Earth.
- U: How long would it take me to get there.
- A: It would take you at least 10 years, since there is currently no rocket able to carry you there. You would have to invent your own.
- U: Is there life on Venus?
- A: Sorry, I don't know.
- U: Ok, thanks.

- A: Hello, I know something about our Solar system. What do you want to ask?
- U: How many planets are there in the Solar system?
- A: There are 8 planets.
- U: Why not 9?
- A: Could you repeat your request?
- U: Can you list the planets in the Solar system?
- A: << lists the planets >>
- U: What about Pluto?
- A: Pluto is a dwarf planet of Solar system. What do you want to ask about Pluto?
- U: Nothing, thanks.
- A: No problem.

- A: Hello, I know something about our Solar system. What do you want to ask?
- U: How far is Mars from Earth?
- A: The distance ranges between xx million kilometers and xx million kilometers depending on their relative orbital offset. Currently, it is xx million kilometers far from Earth.
- U: How long will it take until it is closest to Earth?
- A: It will take xx days.
- U: How long would it take a rocket to get there.
- A: It would take you at least 10 years, since there is currently no rocket able to carry you there. You would have to invent your own.
- U: Didn't we land on Mars already?
- A: Humans have never landed on Mars. They however send probes there several times. Do you want me to list the missions?
- U: No, thanks.
- A: No problem.

- A: Hello, I know something about our Solar system. What do you want to ask?
- U: What is the biggest planet.
- A: Jupiter is the biggest planet in the Solar system. It weights ...
- U: Is there a life on Jupiter?
- A: There is no life on Jupiter that we are aware of.
- U: Could humans live there?
- A: That is very unlikely. Jupiter is gas giant. There is not a physical surface you could live on. Also, Jupiter is the fastest rotating planet. The rotation creates powerful winds. Deep in the atmosphere, the atmospheric pressure is too high for any habitat.
- U: No, thanks.
- A: No problem.

- A: Hello, I know something about our Solar system. What do you want to ask?
- U: How many planets support life?
- A: There is one planet in the Solar system that supports life as we know it. It is planet Earth.
- U: How many planets have lower gravity?
- A: That would be 3. Mercury, Venus and Mars.
- U: What is best place for humans to live except for the Earth.
- A: It would be either the clouds of Venus or Mars. However, Europa also shows promising conditions to support human habitats. There is large ammount of water there. It even has oxygen atmosphere.
- U: What is Europa?
- A: Europa is one of the moons of Jupiter.
- U: How big is it?
- A: The mass of Europa is 0.008 that of Earth.
Ok, thanks.

- A: Hello, I know something about our solar system. What do you want to ask?
- U: I want to ask about our planet?
- A: Do you mean planet Earth?
- U: Yes.
- A: What is its size?
- A: In terms of the diameter it would be 6378 kilometers, however, planet Earth is not a perfect sphere, but more of a flatenned sphere.
- U: How many planets are smaller then Earth?
- A: It would be 3. Mars, Mercury and Venus.
- U: How far is Venus from us.
- A: The distance ranges between 38 million kilometers and 261 million kilometers depending on their relative orbital offset. Currently, it is 132 million kilometers far from Earth.
- U: How long would it take me to get there.
- A: It would take you at least 10 years, since there is currently no rocket able to carry you there. You would have to invent your own.
- U: Is there life on Venus?
- A: Sorry, I don't know.
- U: Ok, thanks.

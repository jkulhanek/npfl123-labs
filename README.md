# NPFL123-labs

This repository contains the pages with lab homeworks for the [UFAL Dialogue Systems course](http://ufal.cz/npfl123), 2020 run.

The individual homework assignments will be listed on separate webpages below. The *deadline* for each homework is 12 days from the day it was assigned (i.e., Sunday next week), 23:59 CET. Additional homework will be assigned for missed deadlines.

To turn in your homework, create a pull request with a subdirectory with your name under the respective directory (e.g., [hw1](hw1/)). You can commit just a file containing a link to your own repo if you like/need to. The preferred format is Markdown for texts and Python for code, plus anything you need for other types of data.

Since this is basically the first run of this type of lab exercises, everything may change in the meantime, but you'll be kept informed.

### Assignments

* [Lab 1 – Feb 18](hw1/README.md) -- deadline: Mar 1
* There will be no labs in the 2nd week of the semester
* [Lab 2 – Mar 3](hw2/README.md) -- deadline: Mar 15
* [Lab 3 – Mar 10](hw3/README.md) -- deadline: Mar 22
* [Lab 4 – Mar 17](hw4/README.md) -- deadline: Mar 29



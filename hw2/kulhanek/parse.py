#!/usr/bin/python
import os
import numpy as np
from argparse import ArgumentParser
from functools import reduce
from itertools import chain
from collections import Counter, deque
import math
import re

def load_task_file(task, set_type = 'train'):
    assert set_type in ['train', 'dev', 'test']
    assert 'DATASETS' in os.environ
    set_type = dict(train='trn', dev='dev',test='tst')[set_type]
    basepath = os.path.join(os.environ['DATASETS'], 'dialog-bAbI-tasks')
    filename_candidates = [x for x in os.listdir(basepath) if x.endswith(f"{set_type}.txt") and x.startswith(f'dialog-babi-task{task}-')]
    assert len(filename_candidates) == 1
    filename = filename_candidates[0]
    return open(os.path.join(basepath, filename), 'r')

def count_sentences(x):
    x = x.strip()
    if x == '':
        return 0
    return 1 + x.count(' . ') + x.count('?')

split_expr = re.compile(r'(\.|;|,|\?|!|\s)\s*')
def collect_words(s):
    s = s.strip()
    sep = ['.',',','!','?', '-',':',';']
    
    words = (x for x in split_expr.split(s) if len(x) > 0 and x not in sep and not '_' in x and not x == ' ')
    for w in words:
        yield w

def count_words(s):
    return sum(1 for _ in collect_words(s))

def vocabulary_size(string_iter):
    def collect_agg(counter, s):
        counter.update(collect_words(s))
        return counter
    return len(reduce(collect_agg, string_iter, set()))


def ngram_iterator(iterator, n):
    buf = deque(maxlen = n)
    for x in iterator:
        buf.append(x)
        if len(buf) == n:
            yield tuple(buf)
    
def entropy(string_iter):
    length = 0
    def collect_agg(counter, s):
        nonlocal length
        counter.update(collect_words(s))
        length += len(s)
        return counter

    word_occ = reduce(collect_agg, string_iter, Counter())
    x = np.array([v / length for x,v in word_occ.items()], np.float32)
    return -np.sum(x * np.log2(x))

def ngram_entropy(string_iter_fn, n):
    length = 0
    def collect_agg_cond(counter, s):
        counter.update(ngram_iterator(collect_words(s), n))
        return counter
    def collect_agg(counter, s):
        nonlocal length
        length += len(s)
        counter.update(ngram_iterator(collect_words(s), n - 1))
        return counter
    counter_cond = reduce(collect_agg_cond, string_iter_fn(), Counter()) 
    counter = reduce(collect_agg, string_iter_fn(), Counter()) 
    entropy = 0
    for k, freq_cond in counter_cond.items():
        freq = counter[k[:-1]]
        entropy -= freq_cond * math.log2(freq_cond / freq)
    return entropy / length



def print_individual_statistics(iterator_fn, label = 'combined'):
    sentences = [count_sentences(a) for a in iterator_fn()]
    words = [count_words(a) for a in iterator_fn()]
    voc_size = vocabulary_size(iterator_fn())
    print(f'{label}:')
    prefix = '   '
    print(f'{prefix}sentences:')
    print(f'{prefix}   total: {np.sum(sentences)}')
    print(f'{prefix}   mean: {np.mean(sentences):0.2f}')
    print(f'{prefix}   std: {np.std(sentences):0.2f}')
    print(f'{prefix}words:')
    print(f'{prefix}   total: {np.sum(words)}')
    print(f'{prefix}   mean: {np.mean(words):0.2f}')
    print(f'{prefix}   std: {np.std(words):0.2f}')
    print(f'{prefix}vocabulary size: {voc_size}')
    print(f'{prefix}entropy: {entropy(iterator_fn()):0.4f}')
    print(f'{prefix}bigram conditional entropy: {ngram_entropy(iterator_fn, 1):0.4f}')

def print_statistics(dataset):
    turns = [len(x) for x in dataset]
    print(f'dialogs: {len(dataset)}')
    print(f'turns:')
    print(f'   total: {np.sum(turns)}')
    print(f'   mean: {np.mean(turns):0.2f}')
    print(f'   std: {np.std(turns):0.2f}')
    combined_iter_fn = lambda: chain(*zip(*(turn for dialog in dataset for turn in dialog)))
    user_iter_fn = lambda: (user for dialog in dataset for i, (user, _) in enumerate(dialog) if user != '' or i > 0)
    system_iter_fn = lambda: (system for dialog in dataset for _, system in dialog)
    print_individual_statistics(combined_iter_fn, 'combined')
    print_individual_statistics(user_iter_fn, 'user')
    print_individual_statistics(system_iter_fn, 'system')

def parse_dataset(f):
    dataset = []
    dialog = []
    next_line = 1
    api_call = False
    for i,line in enumerate(f):
        if not ' ' in line:
            continue
        index = line.index(' ')
        if index <= 0:
            continue
        if next_line != int(line[:index]):
            dataset.append(dialog)
            dialog = []
            api_call = False
        next_line = int(line[:index]) + 1
        line = line[index + 1:]
        if api_call:
            if '\t' in line:
                api_call = False
            else:
                continue
        user, system = tuple(line.rstrip('\n').split('\t'))
        user, system = user.strip(), system.strip()
        if system.startswith('api_call'):
            system = ''
            api_call = True
        if user == '<SILENCE>':
            user = ''
        if user == '' and len(dialog) > 0:
            user, system0 = dialog[-1]
            dialog[-1] = (user, system if system0 == '' else f'{system0} {system}')
        else:
            dialog.append((user, system))
    return dataset

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('task', type=int,help= 'Task to load')
    parser.add_argument('--set', type=str, default='train', help='"train", "dev", or "test"')
    args = parser.parse_args()
    with load_task_file(args.task, args.set) as f:
        data = parse_dataset(f)
    print_statistics(data)


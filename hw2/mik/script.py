import sys
from collections import Counter
import math

if len(sys.argv) != 2:
    print("use filename as argument")

user = list()
system = list()

dialog = (list(), list())
system_sentences = list()
with open(sys.argv[1], mode="r") as file:
    for line in file:
        line = line.rstrip("\n")
        if line:
            line = line.split("\t")
            if len(line) == 2:
                user_sentence = line[0].split(" ", 1)[1]
                if user_sentence == "<SILENCE>":
                    if not dialog[0]:
                        dialog[0].append("")
                    system_sentences.append(line[1])
                else:
                    dialog[0].append(user_sentence)
                    if system_sentences:
                        dialog[1].append("\t".join(system_sentences))
                    system_sentences = [line[1]]
        else:
            dialog[1].append("\t".join(system_sentences))
            system_sentences = list()
            user.append(dialog[0])
            system.append(dialog[1])
            dialog = (list(), list())

dialog_count = len(user) # = len(system)
turn_count = 0
turn_square_count = 0
user_word_count = 0
user_word_square_count = 0
system_sentence_count = 0
system_sentence_square_count = 0
system_word_count = 0
system_word_square_count = 0

for user_turns, system_turns in zip(user, system):
    turn_count += len(user_turns)
    turn_square_count += len(user_turns) ** 2
    for user_turn in user_turns:
        user_word_count += len(user_turn.split())
        user_word_square_count += len(user_turn.split()) ** 2
    for system_turn in system_turns:
        system_sentence_count += len(system_turn.split("\t"))
        system_sentence_square_count += len(system_turn.split("\t")) ** 2
        system_word_count += len(system_turn.split())
        system_word_square_count += len(system_turn.split()) ** 2

print("dialogs", dialog_count, sep='\t')
print("turns", turn_count, sep='\t')
print("system sentences", system_sentence_count, sep='\t')
print("user words", user_word_count, sep='\t')
print("system words", system_word_count, sep='\t')

print("turns per dialog mean", turn_count / dialog_count , sep='\t')
print("turns per dialog dev",  (turn_square_count / dialog_count - (turn_count / dialog_count) ** 2) ** 0.5 , sep='\t')
print("system sentences per turn mean", system_sentence_count / turn_count , sep='\t')
print("system sentences per turn dev", (system_sentence_square_count / turn_count - (system_sentence_count / turn_count) ** 2) ** 0.5 , sep='\t')
print("user words per turn mean", user_word_count / turn_count , sep='\t')
print("user words per turn dev",  (user_word_square_count / turn_count - (user_word_count / turn_count) ** 2) ** 0.5 , sep='\t')
print("system words per turn mean", system_word_count / turn_count , sep='\t')
print("system words per turn dev",  (system_word_square_count / turn_count - (system_word_count / turn_count) ** 2) ** 0.5 , sep='\t')

for collection, name in [(user, "user"), (system, "system")]:
    words = [word for dialog in collection for turn in dialog for word in turn.split()]
    word_counts = Counter(words)
    print(name + " vocabulary size", len(word_counts), sep='\t')
    entropy = sum([count * math.log(len(words) / count) for count in word_counts.values()]) / len(words)  # XXX log2
    print(name + " entropy", entropy, sep='\t')
    bigram_counts = Counter(zip(words, words[1:]))  # XXX this crosses turns ??
    bigram_entropy = sum([count * math.log(word_counts[bigram[0]] / count) for bigram, count in bigram_counts.items()]) / len(words)  # XXX log2
    print(name + " conditional entropy", bigram_entropy, sep='\t')

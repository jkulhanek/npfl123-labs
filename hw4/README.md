# NPFL123 Labs Homework 4, deadline: March 29th

In this assignment, you will build and evaluate a statistical Natural Language Understanding component on the [DSTC2 data](http://camdial.org/~mh521/dstc/).
For completing it, you will use our prepared [Dialmonkey dialogue framework](https://gitlab.com/ufal/dsg/dialmonkey) so you can test the outcome directly.

## What should you do

1. Get the [training](dstc2-nlu-train.json), [development](dstc2-nlu-dev.json) and [test](dstc2-nlu-test.json) data from this directory.

2. Implement a script that trains a statistical model to predict DAs. It shouldn't predict the DA as a single classifier, rather it should
   classify the value for each intent-slot pair where applicable (e.g. _inform(food)_ has multiple possible values) and classify a 
   binary 0-1 for each intent-slot pair that can't have different values (e.g. _request(price)_ or _bye()_ ).

   Don't forget that for the multi-value slots, you'll need a “null” value too.

   You can use any kind of statistical classifier you like, with any library of your choice (e.g. Sklearn, Tensorflow, Pytorch).

   Note that we're not doing slot tagging since the words in the texts aren't tagged with slot values.

3. Train this model on the training set you downloaded. You can use the development set for parameter tuning.
   Using [`dialmonkey.DA.parse_cambridge_da()`](https://gitlab.com/ufal/dsg/dialmonkey/-/blob/master/dialmonkey/da.py#L153) should help you get the desired DA values out of the textual representation.
   Do not look at the test set at this point!

4. Evaluate your model on the test set and write a report with intent-slot pair precision, recall and F1. **TODO: we'll provide you with an evaluation script.**

   Non-null values count as positives, null values count as negatives. Whether they're true or false depends on whether they're correctly predicted.
   Report values for each individual intent-slot pair as well as an average.

6. Implement a module in Dialmonkey that will load your NLU model and work with inputs in the restaurant domain.
   Create a copy of the [nlu_test.yaml](https://gitlab.com/ufal/dsg/dialmonkey/-/blob/master/conf/nlu_test.yaml) config file to work with your new NLU.


Commit the Dialmonkey NLU module and your config file to the dialmonkey repository. Commit the training script and evaluation report to this repository 
(under this directory into a folder with your name).


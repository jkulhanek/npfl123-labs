# NPFL123 Labs Homework 3, deadline: March 22th

In this assignment, you will design a dialogue system component for Natural Language Understanding in a specific domain.
For completing it, you will use our prepared [Dialmonkey dialogue framework](https://gitlab.com/ufal/dsg/dialmonkey) so you can test the outcome directly.

## What should you do

1. Clone the [Dialmonkey repository](https://gitlab.com/ufal/dsg/dialmonkey/-/tree/master). Read the README and look around a bit to get familiar with the code.

2. Recall the domains you picked in the [first homework assignment](../hw1/) and choose one of them.

3. Think of the set of dialogue acts suitable to describe this domain, i.e., list all intents, slots and values that will be needed (some slots
   may have open sets of values, e.g. “restaurant name”, “artist name”, “address” etc.). Write them down.

4. Create a component in the dialogue system that:
  - inherits from [`dialmonkey.component.Component`](https://gitlab.com/ufal/dsg/dialmonkey/-/blob/master/dialmonkey/component.py)
  - is placed under `dialmonkey.nlu.your_domain_nlu`
  - implements a **rule-based** NLU for your domain, yields Dialogue Act items you designed in step 3 (as [`DA`](https://gitlab.com/ufal/dsg/dialmonkey/-/blob/master/dialmonkey/da.py) objects).

5. Create a config file for your domain in the [`conf`](https://gitlab.com/ufal/dsg/dialmonkey/-/tree/master/conf) directory. 
   You can use the [`nlu_test.yaml`](https://gitlab.com/ufal/dsg/dialmonkey/-/blob/master/conf/nlu_test.yaml) file as a starting point (since you don't have any reasonable dialogue policy at this point).

6. Write at least 15 utterances that demonstrate the functionality of your class (a tab-separated file with input + corresponding desired NLU result, one-per-line). 
   The test utterances can (but don't have to) be taken over from the example dialogues you wrote earlier for your domain.

Commit your NLU code and config file to the dialmonkey repository. Commit your lists of dialogue acts and sample utterances to this repository.
